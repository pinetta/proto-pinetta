# proto-pinetta

A prototype of Pinetta for hashing out some of the basics before ascending to the Big Time&trade;

Join us on Matrix at https://matrix.to/#/#pinetta:matrix.org

![Chat](https://img.shields.io/matrix/pinetta:matrix.org)  ![Follow](https://img.shields.io/mastodon/follow/109386276818343749?domain=https%3A%2F%2Ffosstodon.org&style=social)

## Requirements

**Pre-alpha:**

- Database schema
- Account creation & log in/out, delete account
- Settings: Notification, privacy/safety settings
- Pins, comments, boards: Create, add/remove, delete, follow and like
  - Pin types: Text, image/GIF, video
- Markdown compatibility for editing
- Federation: Early ActivityPub implementation (specifics TBD), home/local/federated timelines
- Post federation/privacy
- Community safety features: Follow requests, report, ignore, block, ban
- Accessibility: Low animation mode; image description warning

**Alpha phase:**

- Initial public version
- UI improvements, dark mode
- Community safety features: Admin tools, instance block / allow lists
- Board collaboration: Board management, permissions (owner, moderator, contributor), join/leave, invite
- Following/follower management
- Accessibility: High contrast, colourblind modes

## Getting Started

### System Dependencies

- **Python**
  
  Ensure you have Python 3.11 or later installed with: `python --version`. Visit [python.org](https://www.python.org/) for more information.

### Project Dependencies

#### Python Dependencies

***NOTE:** The following information applies to the [develop](https://codeberg.org/pinetta/proto-pinetta/src/branch/develop) branch and its sub-branches. To run the app, please ensure you pull from this branch.*

----

Virtual environments help separate dependencies between projects.

*Based on [Installing packages using pip and virtual environments &#8212; Python Packaging User Guide](https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/).*

*If you have Python 2 and Python 3 installed, replace `python` with `python3`*

Create a virtual environment by running `python -m venv venv` in the repository root.

To activate the virtual environment, run:

`source venv/bin/activate`

You can deactivate the virtual environment by running `deactivate`.

While the virtual environment is activated, you can install project dependencies by running the following:

`python -m pip install -r requirements.txt`

`pip install -r requirements.txt` should work as well.

If you install any dependencies, run `pip freeze > requirements.txt` while the virtual environment is active.

#### Gitflow

To learn about gitflow, a good place to start are these reads: [A successful Git branching model &raquo; nvie.com](https://nvie.com/posts/a-successful-git-branching-model/) and [Gitflow Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow).

The following will be used:

- main: stores the official release history
- develop: serves as an integration branch for features
- feature/feature-name: branches off of develop and should only contain one feature per branch
- release: for finalizing a release: focusing on bug fixes and preventing the addition of new features
- hotfix: used to quickly patch production releases
- bugfix: to fix bugs of features already merged into develop

There are git extensions to help manage the workflow. For example, Ubuntu users should be able to install the `git-flow` package. Other operating systems will vary in support. **This extension is not required to implement gitflow in your project, but it helps.**

### Version Numbering

**TBD**